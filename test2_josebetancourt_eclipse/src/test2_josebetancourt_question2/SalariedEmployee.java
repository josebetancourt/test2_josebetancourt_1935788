package test2_josebetancourt_question2;

public class SalariedEmployee implements Employee {
	private int yearlySalary;
	
	SalariedEmployee(int yearlySalary){
		this.yearlySalary = yearlySalary;
	}
	
	@Override
	public int getYearlyPay() {
		return this.yearlySalary;
	}
	
}
