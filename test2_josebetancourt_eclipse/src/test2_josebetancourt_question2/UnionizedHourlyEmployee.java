package test2_josebetancourt_question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int pensionContribution;

	UnionizedHourlyEmployee(int weekHours, int hourlyPay, int pensionContribution){
		super(weekHours, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	
	@Override
	public int getYearlyPay() {
		int yearlyPay = getWeekHours() * getHourlyPay() * 52 + this.pensionContribution;
		return yearlyPay;
	}
	
}
