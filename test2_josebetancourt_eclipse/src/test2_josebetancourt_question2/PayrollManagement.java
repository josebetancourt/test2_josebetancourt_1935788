package test2_josebetancourt_question2;

public class PayrollManagement {
	
	public static int getTotalExpenses(Employee[] employee_arr) {
		int totalExpenses = 0;
		
		for(int i=0; i < employee_arr.length; i++) {
			totalExpenses = totalExpenses + employee_arr[i].getYearlyPay();
		}
		
		return totalExpenses;
	}

	public static void main(String[] args) {
		Employee[] employee_arr = new Employee[5];
		
		employee_arr[0] = new SalariedEmployee(100000);
		employee_arr[1] = new HourlyEmployee(20, 13);
		employee_arr[2] = new UnionizedHourlyEmployee(40, 10, 5000);
		employee_arr[3] = new SalariedEmployee(80000);
		employee_arr[4] = new HourlyEmployee(30, 20);
		
		int totalExpenses = getTotalExpenses(employee_arr);
		System.out.println(totalExpenses);
	}

}
