package test2_josebetancourt_question2;

public class HourlyEmployee implements Employee{
	private int weekHours;
	private int hourlyPay;
	
	HourlyEmployee(int weekHours, int hourlyPay){
		this.weekHours = weekHours;
		this.hourlyPay = hourlyPay;
	}
	
	public int getWeekHours() {
		return this.weekHours;
	}

	public int getHourlyPay() {
		return this.hourlyPay;
	}

	@Override
	public int getYearlyPay() {
		int yearlyPay = this.weekHours * this.hourlyPay * 52;
		return yearlyPay;
	}
	
}
