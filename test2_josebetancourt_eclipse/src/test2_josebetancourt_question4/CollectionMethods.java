package test2_josebetancourt_question4;

import java.util.ArrayList;
import java.util.Collection;

import test2_josebetancourt_question3.Planet;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		
		Collection<Planet> firstThreePlanets = new ArrayList<Planet>();
		
		
		for (Planet p : planets) {
			if(p.getOrder() < 4) {
				firstThreePlanets.add(p);
			}
		}
		return firstThreePlanets;
		
	}
	
//	public static void main(String[] args) {
//		Collection<Planet> firstThreePlanets = new ArrayList<Planet>();
//		
//		Planet planetOne = new Planet("Solar System", "One", 1, 100);
//		Planet planetTwo = new Planet("Solar System", "Two", 2, 50);
//		Planet planetThree = new Planet("Solar System", "Three", 3, 80);
//		Planet planetFour = new Planet("Solar System", "Four", 4, 70);
//		
//		firstThreePlanets.add(planetOne);
//		firstThreePlanets.add(planetTwo);
//		firstThreePlanets.add(planetThree);
//		firstThreePlanets.add(planetFour);
//		
//		System.out.println(getInnerPlanets(firstThreePlanets).toString());
//		
//		
//	}
	

}
